FROM python:3.7

WORKDIR /application
RUN mkdir /application/app
COPY /app/* /application/app/
COPY app.py /application
RUN pip3 install -r /application/app/req.txt
EXPOSE 8080
CMD ["python3","/application/app.py"]